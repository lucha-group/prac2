#include <iostream>
#include <string>
#include <exception>

using namespace std;

class ExceptionSumL {
private:
    string error;

public:
    ExceptionSumL(string error)
            : error(error) {
    }

    string getError() { return error; }
};

class ExceptionSumR {
private:
    string error;

public:
    ExceptionSumR(string error)
            : error(error) {
    }

    string getError() { return error; }
};


class ExceptionMultR {
private:
    string error;

public:
    ExceptionMultR(string error)
            : error(error) {
    }

    string getError() { return error; }
};

class MyException : public exception {
public:
    const char *what() const throw() {
        return "L Multiply Exception";
    }
};

template<typename T>
class StrangeNumber {

private:
    T value;
public :
    StrangeNumber(T value) : value(value) {}

    T getvalue() {
        return value;
    }

    friend int operator+(StrangeNumber<T> &a, int b) {
        return a.value + b;
    }

    friend int operator+(int a, StrangeNumber<T> &b) {
        return a + b.value;
    }

    friend int operator*(StrangeNumber<T> &a, int b) {
        return a.value * b;
    }

    friend int operator*(int a, StrangeNumber<T> &b) {
        return a * b.value;
    }


};

int main() {
    cout << "Hello, World!" << std::endl;
    StrangeNumber<int> ta(15);
    try {
        int c1 = ta + 5;
        if (c1 > 10)
            throw ExceptionSumR("Right sum exception");
    }
    catch (ExceptionSumR exception) {
        cout << exception.getError() << endl;

    }

    try {
        int c1 = ta + 5;
        if (c1 > 8)
            throw ExceptionSumL("Left sum exception");
    }
    catch (ExceptionSumL exception) {
        cout << exception.getError() << endl;

    }

    try {
        int c1 = ta * 5;
        if (c1 > 8)
            throw ExceptionMultR("Right multiply exception");
    }
    catch (ExceptionMultR exception) {
        cout << exception.getError() << endl;

    }

    try {
        int c1 = ta * 5;
        if (c1 > 8)
            throw MyException();
    }
    catch (MyException &e) {
        cout << e.what() << endl;

    }

    try {
        if (5 > 3) {
            string a = "other exception";
            throw a;
        }
    }
    catch (string a)
    {
        cout<<a<<endl;
    }


    try
    {
        if (5 > 3) {
            string a = "5>3";
            throw a;
        }}
    catch (string a)
    {
        cout<<a<<endl;
        try
        {
            if (5 > 4) {
                string b = "and 5>4";
                throw b;
            }
        }
        catch (string b)
        {
            cout<<b<<endl;
        }
    }
    return 0;
}
